<?php

require_once ('animal.php');
require_once ('Ape.php');
require_once ('Frog.php');

// Release 0

$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "legs : " . $sheep->legs . "<br>"; // 4
echo "cold blooded  : " . $sheep->cold_blooded . "<br> <br>"; // "no"




//Release 1

$kodok = new Frog("buduk");

echo "Name : $kodok->name <br>"; 
echo "legs : $kodok->legs <br>"; 
echo "cold blooded  : $kodok->cold_blooded <br>"; 
$kodok->jump();



echo "<br><br>";




$sungokong = new Ape("kera sakti");
 
echo "Name :  $sungokong->name <br>"; 
echo "legs :  $sungokong->legs <br>"; 
echo "cold blooded  :  $sungokong->cold_blooded <br>"; 
$sungokong->yell();


?>